﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCareOT.API
{
    public class UserRegistrationRequest
    {
        [JsonProperty("email")]
        public String Email { get; set; }

        [JsonProperty("firstName")]
        public String FirstName { get; set; }

        [JsonProperty("lastName")]
        public String LastName { get; set; }

        [JsonProperty("accountType")]
        public String AccountType { get; set; }
    }
}
