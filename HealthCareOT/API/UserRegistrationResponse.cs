﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HealthCareOT.API
{
    public class UserRegistrationResponse
    {
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("detail")]
        public string Detail { get; set; }
        [JsonProperty("password")]
        public string Password { get; set; }

    }
}
