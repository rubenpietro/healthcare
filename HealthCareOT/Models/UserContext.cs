﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace HealthCareOT.Models
{
    public class UserContext : DbContext
    {
        public UserContext(DbContextOptions options)
    : base(options)
        {
        }

        //public UserContext() { }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseInMemoryDatabase("InMemoryProvider");
        //}

        public DbSet<User> Users { get; set; }

        protected override void  OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasKey(c => c.Email);
        }
    }
}
