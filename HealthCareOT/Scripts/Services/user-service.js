﻿import trae from 'trae'

//trae documents https://github.com/Huemul/trae
trae.defaults({
    post: {
        headers: {
            'Content-Type': 'application/json',
            'timeout': '300000'
        }
    },
    get: {
        headers: {
            'Content-Type': 'application/json'
        }
    }
});

const appService = trae.create({

    baseUrl: "https://localhost:44327/Api/Users"
})

const myServices = {}

myServices.registerUser = function (user) {
    return appService.post('/RegisterUser', {
        "email": user.email,
        "firstName": user.firstName,
        "lastName": user.lastName,
        "accountType": user.accountType
    }).then(res => res.data);
}


export default myServices