﻿import Vue from 'vue'

//Generador de pantalla de login
import LoginUser from '../Components/LoginUser.vue'
import RegisterUser from '../Components/RegisterUser.vue'

var app;
app = new Vue({
    el: '#app',
    components: {
        'vs-login-user': LoginUser,
        'vs-register-user': RegisterUser
    }
});