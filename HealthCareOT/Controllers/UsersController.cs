﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using HealthCareOT.API;
using HealthCareOT.Models;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace HealthCareOT.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly UserContext _caregiver;
        private readonly UserContext _patient;
        private readonly UserContext _both;
        // GET: /<controller>/


        public UsersController(UserContext context)
        {
            _caregiver = context;
            _patient = context;
            _both = context;
        }


        [HttpPost("RegisterUser")]
        public ActionResult<UserRegistrationResponse> RegisterUser(UserRegistrationRequest request)
        {
            var response = new UserRegistrationResponse();            

            try
            {
                if (request == null)
                    throw new Exception("request is null");

                var msgMissingParams = "";

                if (string.IsNullOrEmpty(request.Email))
                    msgMissingParams += " Email,";

                if (string.IsNullOrEmpty(request.FirstName))
                    msgMissingParams += " FirstName,";

                if (string.IsNullOrEmpty(request.LastName))
                    msgMissingParams += " LastName,";

                if (string.IsNullOrEmpty(request.AccountType))
                    msgMissingParams += " AccountType,";

                if (!string.IsNullOrEmpty(msgMissingParams))
                {
                    msgMissingParams = msgMissingParams.Substring(0, msgMissingParams.Length - 1);
                    throw new Exception("Following fields are missed: " + msgMissingParams);
                }

                Random randomObj = new Random();
                var pass = randomObj.Next();
                var user = new User()
                {
                    Email = request.Email,
                    FirstName = request.FirstName,
                    LastName = request.LastName,
                    AccountType = request.AccountType,
                    Password = pass.ToString()
                };

                //Validatión if the user already exist
                var existCaregiver = _caregiver.Users.SingleOrDefault(u => u.Email == request.Email);
                var existPatient = _patient.Users.SingleOrDefault(u => u.Email == request.Email);
                var existBoth = _both.Users.SingleOrDefault(u => u.Email == request.Email);


                if (existCaregiver == null && existPatient == null && existBoth == null)
                {
                    AddUser(user);
                }
                else
                {
                    throw new Exception("The user already exists");
                }

                response.Error = "0";
                response.Detail = "User registered succesfully";
                response.Password = user.Password;

                return response;
            } catch (Exception ex)
            {
                response.Error = "1";
                response.Detail = ex.Message.ToString(); ;
                return response;
            }


        }


        private void AddUser(User user)
        {
            if(user.AccountType == "caregiver")
            {
                _caregiver.Add(user);
                _caregiver.SaveChanges();

            }else if (user.AccountType == "patient")
            {
                _patient.Add(user);
                _patient.SaveChanges();
            }
            else
            {
                _both.Add(user);
                _both.SaveChanges();
            }
        }
    }
}
